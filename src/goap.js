// if the module has no dependencies, the above pattern can be simplified to
(function (root, factory) {
    if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like enviroments that support module.exports,
        // like Node.
        module.exports = factory();
    } else if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory);
    } else {
        // Browser globals (root is window)
        root.GOAP = factory();
  }
}(this, function () {

    //////////////////////////////////////////
    // GOAP
    //////////////////////////////////////////
    var GOAP = (function() {
      var Graphs    = {};
      var Actionset = {};
      
      function hashString(str) {
        var hash = 0;
        if (str.length == 0) return hash;
        for (i = 0; i < str.length; i++) {
          char = str.charCodeAt(i);
          hash = ((hash<<5)-hash)+char;
          hash = hash & hash; // Convert to 32bit integer
        }
        return hash;
      };
      
      return {
        initialize: function(global_actionset) {
          Actionset = global_actionset;
        },

        getActionset: function() {
          return Actionset;
        },
        
        getGraph: function(actions) {
          if (!actions || actions.length == 0) return;

          var hash = hashString(actions.sort().join(''));
          
          if (Graphs[hash] === undefined) {
            var actionset = {};
            for (var i in actions) {
              actionset[actions[i]] = Actionset[actions[i]];
            }
            Graphs[hash] = new Graph(actionset);
          }
          
          return Graphs[hash];
        }    
      };
    })();
    
    //////////////////////////////////////////
    // State
    //////////////////////////////////////////
    var State = GOAP.State = function(initial) {
      this.keys = initial;
    };
    
    State.prototype = {
      toString: function() {
        var str = [];
        this.forEachKey(function(value, key, keys) {
          str.push(key +': '+ value);  
        });
        return str.join('; ');
      },

      forEachKey: function(callback, context) {
        context = context || this;
        var result;
        
        for (var i in this.keys) {
          if (this.keys[i] !== undefined) {
            result = callback.call(context, this.keys[i], i, this.keys);
            if (result !== undefined) return result;
          }
        }
      },
      
      clone: function() {
        var keys = {};
        this.forEachKey(function(value, key, list) {
          keys[key] = value;
        });
        return new State(keys);
      },
      
      distanceTo: function(state) {
        return Object.keys(state.difference(this).keys).length;
      },
      
      /**
       * Returns all of the keys in `this` that are not present in the comparison.
       *
       * a == [a, b, c, d];
       * b == [d, e, f];
       * a.difference(b) == [a, b, c];
       * b.difference(a) == [e, f]
       */
      difference: function(state) {
        var keys = {};
        var state = (state instanceof State) ? state.keys : state;
        
        this.forEachKey(function(value, key, list) {
          if (state[key] !== value) keys[key] = value;
        });
        
        return new State(keys);
      },
      
      /**
       * Returns a new State with the keys of each state merged.
       *
       * a == [a, b, c, d];
       * b == [d, e, f];
       * a.union(b) == [a, b, c, d, e, f];
       * b.union(a) == [d, e, f, a, b, c]
       */
      union: function(state) {
        var clone = this.clone();
        var state = (state instanceof State) ? state.keys : state;
        
        for (var key in state) {
          if (clone.keys[key] !== state[key]) clone.keys[key] = state[key];
        };
        
        return clone;
      }
    };
    
    //////////////////////////////////////////
    // Node
    //////////////////////////////////////////
    var Node = GOAP.Node = function(id, goal, current) {
      this.id = id;
      
      this.scores = {'g': 0, 'f': 0, 'h': 0};
      
      this.goal;
      this.current;
      
      this.setState(goal || {}, current || {});
    };
    
    Node.prototype = {
      toString: function() {
        var str = [this.id +' NODE -- g:'+ this.score('g') +' -- f:'+ this.score('f')];
        str.push('-- GOAL = '+ this.goal.toString());
        str.push('-- CURRENT = '+ this.current.toString());
        str.push('-- DIFF = '+ this.goal.difference(this.current).toString());
        return str.join("\n") +"\n";
      },

      distanceTo: function(node) {
        return this.goal.distanceTo(node.goal);
      },

      satisfaction: function() {
        return this.current.distanceTo(this.goal);
      },

      satisfied: function() {
        return this.satisfaction() == 0;
      },
      
      setState: function(goal, current) {
        this.goal    = (goal instanceof State)    ? goal.clone()    : new State(goal);
        this.current = (current instanceof State) ? current.clone() : new State(current);
      },
      
      setAction: function(action) {
        this.goal    = this.goal.union(action.conditions);
        this.current = this.current.union(action.effects); 
      },

      score: function(name, value) {
        if (value !== undefined) this.scores[name] = value;
        return this.scores[name];
      }
    };
    
    //////////////////////////////////////////
    // Set
    //////////////////////////////////////////
    var Set = GOAP.Set = function() {
      this.items = [];
    };
    
    Set.prototype = {
      forEach: function(callback, context) {
        context = context || this;
        var result;
        
        for (var i=0, len=this.items.length; i<len; ++i) {
          result = callback.call(context, this.items[i], i, this.items);
          if (result !== undefined) return result;
        }
      },

      get length() {
        return this.items.length;
      },
      
      remove: function(item) {
        this.forEach(function(value, index, items) {
          if (item == value) {
            items.splice(index, 1);
            return false;
          }
        });
      },
      
      has: function(item) {
        return !!this.forEach(function(value, index, items) {
          if (item == value) return true;
        });
      },
      
      getBest: function() {
        var best;
        
        this.forEach(function(item, index, items) {
          if (!best || item.score('f') < best.score('f')) best = item;
        });
        
        return best;
      },
      
      insert: function(item) {
        this.items.push(item);
      }
    };
    
    //////////////////////////////////////////
    // Action
    //////////////////////////////////////////
    var Action = GOAP.Action = function(name, spec) {
      this.name       = name;
      this.conditions = new State(spec.conditions);
      this.effects    = new State(spec.effects);
    };
    
    //////////////////////////////////////////
    // Graph
    //////////////////////////////////////////
    var Graph = GOAP.Graph = function(actionset) {
      this.nodes       = {};
      this.key_effects = {};
      
      // Build the initial graph; no edges
      for (var name in actionset) {
        this.nodes[name] = new Action(name, actionset[name]);
      }
      
      // Build the key-effects table
      for (var name in this.nodes) {
        this.nodes[name].effects.forEachKey(function(value, key, keys) {
          if (this.key_effects[key] === undefined) this.key_effects[key] = [];
          this.key_effects[key].push(name);
        }, this);
      }
    };
    
    Graph.prototype = {
      getRelated: function(keys) {
        var neighbors = [];
        
        for (var key in keys) {
          if (this.key_effects[key]) neighbors = neighbors.concat(this.key_effects[key]);
        }
        
        return neighbors;
      },
      
      getAction: function(id) {
        return this.nodes[id];
      },

      getActions: function() {
        return this.nodes;
      }
    };
    
    //////////////////////////////////////////
    // Planner
    //////////////////////////////////////////
    var Planner = GOAP.Planner = function(entity, action_list) {
      this.entity = entity;
      this.agraph = GOAP.getGraph(action_list);
      this.ngraph = {};
    };
    
    Planner.prototype = {
      getNeighborNodes: function(node) {
        var neighbors = this.getNeighborActions(node);
        var nodes = [];
        var action, id;
        
        for (var i in neighbors) {
          id = neighbors[i];
          if (this.getNode(id) === undefined) {
            action = this.getAction(id);
            if (!action) continue;
            this.ngraph[id] = new Node(id, action.effects, {});
          }
          nodes.push(this.ngraph[id]);
        }
        
        return nodes;
      },
      
      getNeighborActions: function(node) {
        var diff = node.goal.difference(node.current);
        return this.agraph.getRelated(diff.keys);
      },
      
      getNode: function(id) {
        return this.ngraph[id];
      },
      
      getAction: function(id) {
        return this.agraph.getAction(id);
      },

      addEntityState: function(node) {
        var diff = Object.keys(node.goal.difference(node.current).keys);
        var state = this.entity.getState(diff);
        node.current = node.current.union(state); 
      },
      
      buildPlan: function(goal) {
        this.ngraph   = {};
        var planner   = this;
        var openset   = new Set();
        var closedset = new Set();  
        var goal_node = new Node('start', goal, {});
        var current;
        
        this.addEntityState(goal_node);
        
        openset.insert(goal_node);

        goal_node.score('f', goal_node.goal.distanceTo(goal_node.current));
        
        while (openset.length) {
console.log('==============================================================');
          current = openset.getBest();
console.log(current.toString());  

          if (current.satisfied())
            return current; //reconstruct_path(current);
          
          openset.remove(current);
          closedset.insert(current);
          
          var neighbors = planner.getNeighborNodes(current);
          var neighbor;
          var g_score;
          
          for (var i in neighbors) {
            neighbor = neighbors[i];
            g_score = current.score('g') + current.distanceTo(neighbor);
            
            if (closedset.has(neighbor) && g_score >= neighbor.score('g'))
              continue;
              
            if (!openset.has(neighbor) || g_score < neighbor.score('g')) {
              neighbor.parent = current;
              neighbor.setState(current.goal, current.current);
              neighbor.setAction(planner.getAction(neighbor.id));

              this.addEntityState(neighbor);

              neighbor.score('g', g_score);
              neighbor.score('f', g_score + neighbor.goal.distanceTo(neighbor.current));
              
              if (closedset.has(neighbor)) closedset.remove(neighbor);
              openset.insert(neighbor);

console.log('Neighbor ++++++++++++++++ '+ [neighbor]);
            }
            
          }
        }
        
        return false;
      }
      
    };
        
    return GOAP;
}));
