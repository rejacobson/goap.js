describe('GOAP', function() {

  beforeEach(function() {
    GOAP.initialize(window.Actionset);
  });

  it('#initialize should initialize GOAP with a global Actionset', function() {
    expect(Object.keys(GOAP.getActionset()).length).toBeGreaterThan(0);
    expect(GOAP.getActionset()).toEqual(window.Actionset);
  });

  it('#getGraph should create, cache and return Actionset graphs', function() {
    var subset  = Object.keys(window.Actionset).slice(0, 3);
    var graph   = GOAP.getGraph(subset);
    var actions = graph.getActions();
    var keys    = Object.keys(actions);

    expect(keys.length).toEqual(3);
    expect(keys).toEqual(subset);
    expect(graph).toBe(GOAP.getGraph(subset));
  });

  describe('State', function() {
    it('#new accepts an initial state', function() {
        var state = new GOAP.State({a: 'A', b: 'B'});
        expect(state.keys).toEqual({a: 'A', b: 'B'});
    });  
    
    it('#forEachKey executes a callback function on each key/value pair in the state', function() {
      var state = new GOAP.State({a: 'A', b: 'B', c:undefined});
      var expected = {};
      state.forEachKey(function(value, key) { expected[key] = value; });

      expect(expected).toEqual({a: 'A', b: 'B'});
    });

    it('#forEachKey stops iteration if the callback function returns a value', function() {
      var state = new GOAP.State({a: 'A', b: 'B', c: 'C', d: 'D'});
      var expected;
      var return_val;

      // Returns true - stops iteration
      expected = {};
      return_val = state.forEachKey(function(value, key) { expected[key] = value; return true; });
      expect(expected).toEqual({a: 'A'});
      expect(return_val).toEqual(true);

      // Returns false - stops iteration
      expected = {};
      return_val = state.forEachKey(function(value, key) { expected[key] = value; return false; });
      expect(expected).toEqual({a: 'A'});
      expect(return_val).toEqual(false);

      // Returns undefined - continues iteration
      expected = {};
      return_val = state.forEachKey(function(value, key) { expected[key] = value; return undefined; });
      expect(expected).toEqual({a: 'A', b: 'B', c: 'C', d: 'D'});
      expect(return_val).toBeUndefined();
    });

    it('#clone creates and returns a copy of the state', function() {
      var state = new GOAP.State({a: 'A', b: 'B'});
      var clone = state.clone();

      expect(state.keys).toEqual(clone.keys);
      expect(state).not.toBe(clone);
    });

    it('#distanceTo should return the number of keys that are different between two states', function() {
      var a = new GOAP.State({a: 'A', b: 'B', c: 'C'});
      var b = new GOAP.State({c: 'C', d: 'D'});
      var c = new GOAP.State({a: 'Z'});

      expect(a.distanceTo(b)).toEqual(1);
      expect(b.distanceTo(a)).toEqual(2);
      expect(c.distanceTo(a)).toEqual(3);
    });

    it('#difference should return a new State with keys that are not found in the passed in state', function() {
      var a = new GOAP.State({a: 'A', b: 'B', c: 'C'});
      var b = new GOAP.State({c: 'C', d: 'D'});  
      var c = new GOAP.State({a: 'Z', b: 'X', c: 'C'});  

      var diffa = a.difference(b);
      var diffb = b.difference(a);
      var diffc = a.difference(c);

      expect(diffa.keys).toEqual({a: 'A', b: 'B'});
      expect(diffb.keys).toEqual({d: 'D'});
      expect(diffc.keys).toEqual({a: 'A', b: 'B'});
    });

    it('#union should return a new State with keys combined', function() {
      var a = new GOAP.State({a: 'A', b: 'B', c: 'C'});
      var b = new GOAP.State({c: 'C', d: 'D'});  
      var c = new GOAP.State({a: 'Z', b: 'X', c: 'C'});  

      var diffa = a.union(b);
      var diffb = b.union(a);
      var diffc = a.union(c);

      expect(diffa.keys).toEqual({a: 'A', b: 'B', c: 'C', d: 'D'});
      expect(diffb.keys).toEqual({c: 'C', d: 'D', a: 'A', b: 'B'}); 
      expect(diffc.keys).toEqual({a: 'Z', b: 'X', c: 'C'});
    });
  }); // end State

  describe('Action', function() {
    it('#new constructs an Action object with a name and an action spec', function() {
      var spec = window.Actionset['Eat'];
      var action = new GOAP.Action('Eat', spec);

      expect(action.name).toEqual('Eat');

      expect(action.conditions instanceof GOAP.State).toEqual(true);
      expect(action.effects instanceof GOAP.State).toEqual(true);

      expect(action.conditions.keys).toEqual(spec.conditions);
      expect(action.effects.keys).toEqual(spec.effects);
    });
  });

  describe('Node', function() {
    it('#new constructs an empty Node with an id', function() {
      var node = new GOAP.Node('nodeid'); 

      expect(node.id).toEqual('nodeid');
      expect(node.goal.keys).toEqual({});
      expect(node.current.keys).toEqual({});
    });

    it('#new constructs a Node with inital current and goal states', function() {
      var node = new GOAP.Node('nodeid', {a: 'A'}, {b: 'B'});

      expect(node.goal.keys).toEqual({a: 'A'});
      expect(node.current.keys).toEqual({b: 'B'});
    });

    it('#distanceTo returns the number of goal keys that are not found in another node goal', function() {
      var a = new GOAP.Node('a', {a: 'A', b: 'B'}, {});
      var b = new GOAP.Node('b', {a: 'A', b: 'B', c: 'C', d: 'D'}, {});
      var c = new GOAP.Node('c', {a: 'Z'}, {});

      var d1 = a.distanceTo(b);
      var d2 = b.distanceTo(a);
      var d3 = c.distanceTo(a);

      expect(a.distanceTo(b)).toEqual(2);
      expect(b.distanceTo(a)).toEqual(0);
      expect(c.distanceTo(a)).toEqual(2);
      expect(a.distanceTo(c)).toEqual(1);
    });

    it('#satisfaction returns the distance from the Node`s current state to the goal state', function() {
      var a = new GOAP.Node('a', {a: 'A', b: 'B'}, {b: 'B', c: 'C'});
      var b = new GOAP.Node('b', {a: 'A', b: 'B'}, {a: 'Z', b: 'Y', c: 'C'});

      expect(a.satisfaction()).toEqual(1);
      expect(b.satisfaction()).toEqual(2);
    });

    it('#satisfied returns true when the node`s current state is at the goal state', function() {
      var a = new GOAP.Node('a', {a: 'A', b: 'B'}, {a: 'A', b: 'B', c: 'C'});
      expect(a.satisfied()).toEqual(true); 
    });

    it('#satisfied returns false when the node`s current state is NOT at the goal state', function() {
      var a = new GOAP.Node('a', {a: 'A', b: 'B'}, {a: 'Z', b: 'B'});
      expect(a.satisfied()).toEqual(false); 
    });

    it('#setState sets the goal and current states of a node', function() {
      var a = new GOAP.Node('a', {a: 'A', b: 'B'}, {x: 'X', y: 'Y'});

      a.setState({a: 'AA', b: 'BB'}, {x: 'XX'});

      expect(a.goal.keys).toEqual({a: 'AA', b: 'BB'});
      expect(a.current.keys).toEqual({x: 'XX'});
    });

    it('#setState sets the goal and current states of a node using State objects', function() {
      var a  = new GOAP.Node('a', {a: 'A', b: 'B'}, {x: 'X', y: 'Y'});
      var s1 = new GOAP.State({a: 'AA', b: 'BB'});
      var s2 = new GOAP.State({x: 'XX'});

      a.setState(s1, s2);

      expect(a.goal.keys).toEqual({a: 'AA', b: 'BB'});
      expect(a.current.keys).toEqual({x: 'XX'});
    });

    it('#setAction adds the `conditions` and `effects` states from an Action object to the `goal` and `current` states of a Node', function() {
      var a = new GOAP.Node('a', {a: 'A', b: 'B'}, {x: 'X', y: 'Y'});
      var action = new GOAP.Action('action', {conditions: {a:'AA'}, effects: {x: 'XX'}});

      a.setAction(action);

      expect(a.goal.keys).toEqual({a: 'AA', b: 'B'});
      expect(a.current.keys).toEqual({x: 'XX', y: 'Y'});
    });

    it('#score can set or get a Node`s score value', function() {
      var a = new GOAP.Node('a', {}, {});

      a.score('g', 1); 
      a.score('f', 2); 
      a.score('h', 3); 

      expect(a.score('g')).toEqual(1);
      expect(a.score('f')).toEqual(2);
      expect(a.score('h')).toEqual(3);
    }); 
  });

  describe('Set', function() {
    var set;
    var node;

    beforeEach(function() {
      set  = new GOAP.Set();
      node = new GOAP.Node('node', {a: 'A'}, {z: 'Z'});
    });

    it('has a length property', function() {
      expect(set.length).toEqual(0);
      set.insert(node);
      expect(set.length).toEqual(1);
    });

    it('#forEach executes a callback on each item in the set', function() {
      set.insert('a');
      set.insert('b');
      set.insert('c');

      var expected = [];

      set.forEach(function(value, index, items) {
        expected.push(value);
      });

      expect(expected).toEqual(['a', 'b', 'c']);
    });

    it('#forEach stops iteration if the callback function returns a value', function() {
      set.insert('a');
      set.insert('b');
      set.insert('c');

      var expected = [];

      var result = set.forEach(function(value, index, items) {
        expected.push(value);
        return true;
      });

      expect(expected).toEqual(['a']);
      expect(result).toEqual(true);
    });

    it('#insert adds an item to the set', function() {
      set.insert(node);
      expect(set.length).toEqual(1);
      expect(set.items[0]).toBe(node);
    });

    it('#remove deletes an item from the set', function() {
      set.insert(node);
      set.insert('a');
      set.insert('b');

      set.remove('a');

      expect(set.length).toEqual(2); 
      expect(set.items[0]).toBe(node);
      expect(set.items[1]).toBe('b');
    });
    
    it('#has returns true if a set contains an item', function() {
      set.insert('a');
      set.insert('b');
      set.insert('c');

      expect(set.has('b')).toEqual(true);
    });

    it('#has returns false if a set does not contain an item', function() {
      set.insert('a');
      set.insert('b');
      set.insert('c');

      expect(set.has('z')).toEqual(false);
    });

    it('#getBest returns the node with the lowest f_score', function() {
      var a = new GOAP.Node('a');
      var b = new GOAP.Node('b');
      var c = new GOAP.Node('c');

      a.score('f', 3);
      b.score('f', 1);
      c.score('f', 2);

      set.insert(a);
      set.insert(b);
      set.insert(c);

      expect(set.getBest()).toBe(b);
    });
  });

  describe('Graph', function() {
    var graph;

    beforeEach(function() {
      graph = new GOAP.Graph(window.Actionset);
    });

    it('#new processes an actionset into a traversable graph of Action objects', function() {
      expect(graph).toBeDefined();
      expect(graph instanceof GOAP.Graph).toBe(true);
    });

    it('#getActions returns the entire graph structure', function() {
      var actions = graph.getActions();
      var action;

      for (var name in actions) {
        action = actions[name];
        expect(action).toBeDefined();
        expect(action instanceof GOAP.Action).toBeTruthy();
        expect(action.conditions.keys).toEqual(window.Actionset[name].conditions);
        expect(action.effects.keys).toEqual(window.Actionset[name].effects);
      }
    });

    it('#action returns a single action', function() {
      var action = graph.getAction('Eat');
      var bogus_action = graph.getAction('BogusAction');

      expect(action).toBeDefined();
      expect(action instanceof GOAP.Action).toBe(true);
      expect(bogus_action).toBeUndefined();
    });

    it('#getRelated returns all Action names whose effects hash have a matching entry in the passed in keys', function() {
      var actions = graph.getRelated({isTargetDead: true, isHungry: false});

      expect(actions.indexOf('MeleeAttack')).not.toBe(-1);  
      expect(actions.indexOf('MissileAttack')).not.toBe(-1);  
      expect(actions.indexOf('Eat')).not.toBe(-1);  
      expect(actions.indexOf('Wander')).toBe(-1);
    })
  });

  describe('Planner', function() {
    var planner;
    var entity = {
      state: {},
      getState: function(keys) {
        var state = {}, key;
        for (var i in keys) {
          key = keys[i];
          state[key] = this.state[key] || false;
        }
        return state;
      }
    };

    beforeEach(function() {
      var action_list = Object.keys(window.Actionset);
      entity.state = {};
      planner = new GOAP.Planner(entity, action_list);
    });

    it('#new creates a new planner for an Entity and an array of action names', function() {
      expect(planner.entity).toBe(entity);
      expect(planner.agraph instanceof GOAP.Graph).toBe(true); 
    });

    it('#getAction returns an action from the Actionset graph', function() {
      var action = planner.getAction('Eat');
      var bogus_action = planner.getAction('BogusAction');

      expect(action).toBeDefined();
      expect(action instanceof GOAP.Action).toBe(true);
      expect(bogus_action).toBeUndefined();
    });

    it('#getNeighborActions returns a list of neighbor Actions of the passed in Node', function() {
      var node = new GOAP.Node('start', {isHungry:false, isTargetDead:true}, {isTargetDead:true});
      var neighbors = planner.getNeighborActions(node);

      expect(neighbors.length).toBe(2);
      expect(neighbors.indexOf('Eat')).not.toBe(-1);
      expect(neighbors.indexOf('Graze')).not.toBe(-1);
      expect(neighbors.indexOf('MeleeAttack')).toBe(-1);
    });

    it('#getNode returns a node by id', function() {
      planner.ngraph = {
        'start': 'start',
        'middle': 'middle',
        'end': 'end'
      }; 
      expect(planner.getNode('end')).toBe('end');
    });

    it('#getNeighborNodes ', function() {
      var node = new GOAP.Node('start', {isHungry:false}, {});
      var nodes = planner.getNeighborNodes(node);
      var expected_actions = ['Eat', 'Graze'];
      
      expect(nodes.length).toBe(2);

      for (var i in nodes) {
        expect(nodes[i] instanceof GOAP.Node).toBe(true);
        expect(expected_actions.indexOf(nodes[i].id)).not.toBe(-1);
      }
    });

    it('#addEntityState should add the Entity`s state to the node current state.', function() {
      var node = new GOAP.Node('start',
        {isHungry:false, isTargetDead: true, hasFood: true},
        {hasFood: true});
      entity.state = {isHungry: true, isTargetDead: false, hasFood: false};
      planner.addEntityState(node);
      
      expect(node.current.keys.isHungry).toBe(true);
      expect(node.current.keys.isTargetDead).toBe(false);
      expect(node.current.keys.hasFood).toBe(true);
    });

    it('#buildPath', function() {
      entity.state = {isHungry: true};
      var goal = {isHungry: false};
      var result = planner.buildPlan(goal); 
console.log('Planner result --------------------');
console.log(result);
    });
  });
});
